#!/bin/bash

export GPG_TTY=$(tty)

echo "$KEY" > priv.gpg
gpg --import pub.gpg && gpg --batch --import priv.gpg

version="$(wget https://api.github.com/repos/ProtonMail/proton-mail-export/tags -O - -q | grep name | sed 's/.*v/v/' | sed 's/".*//' | head -n 1)"

if [[ $(wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/version.txt) == $version ]]; then
	mkdir -p public/amd64

	cd public/amd64

	wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export-cli

	if [[ ! -f proton-mail-export-cli ]]; then
		wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export-cli
	fi
	
	wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export.so

	if [[ ! -f proton-mail-export.so ]]; then
		wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export.so
	fi

	wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export-cli.asc

	wget https://julianfairfax.codeberg.page/proton-mail-export-prebuilt/amd64/proton-mail-export.so.asc
else
	git clone https://github.com/ProtonMail/proton-mail-export

	cd proton-mail-export

	git fetch --tags

	git checkout tags/$version

	git submodule update --init --recursive
	
	sed -i 's|outputPath / "logs"|"/tmp/proton-mail-export-logs"|' cli/bin/main.cpp
	
	cmake -S. -B build
	
	cmake --build build

	mkdir -p ../public/amd64

	cp ../pub.gpg ../public

	cd build/go-lib
	
	gpg --detach-sig --armor --sign proton-mail-export.so
	
	cd ../cli

	gpg --detach-sig --armor --sign proton-mail-export-cli
	
	cd ../../

	cp build/go-lib/proton-mail-export.so ../public/amd64/

	cp build/go-lib/proton-mail-export.so.asc ../public/amd64/

	cp build/cli/proton-mail-export-cli ../public/amd64/

	cp build/cli/proton-mail-export-cli.asc ../public/amd64/
	
	echo "$version" | tee ../public/amd64/version.txt
	
	echo "$version" | tee ../public/version.txt
fi
